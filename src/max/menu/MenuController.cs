﻿using max.input;
using max.menu.component;
using max.option;
using max.sound.controller;
using System.Collections.Generic;
using System.IO;

namespace max.menu
{
    public class MenuController
    {
        public InputController InputController { get; private set; }
        public Stack<Menu> MenuStack { get; private set; } = new Stack<Menu>();
        public MenuOptions Options { get; private set; } = new MenuOptions();
        public SoundController SoundController { get; private set; } = null;

        int CountDown = -1;

        const int FINAL_COUNTDOWN = 2;
        public MenuController()
        {
            
        }

        public MenuController (SoundController SoundController, InputController InputController)
        {
            this.SoundController = SoundController;
            this.InputController = InputController;
            Validate();
        }

        public void Update()
        {
            if(MenuStack.Count > 0)
            {
                Menu m = MenuStack.Peek();
                m.Update();
            }

            if (CountDown != -1)
            {
                CountDown--;
            }
        }

        public Menu Pop()
        {
            if (CountDown == -1)
            {
                if (MenuStack.Count > 0)
                {
                    Menu m = MenuStack.Pop();
                    m.Dispose();
                    if (MenuStack.Count > 0)
                    {
                        Menu mm = MenuStack.Peek();
                        mm.Reactivate();
                    }

                    CountDown = FINAL_COUNTDOWN;

                    return m;
                }
            }
            return null;
        }

        public void Push(Menu menu)
        {
            if (CountDown == -1)
            {
                if (MenuStack.Count > 0)
                {
                    Menu m = MenuStack.Peek();
                    m.Deactivate();
                }
                menu.Initialize(this);
                MenuStack.Push(menu);
            }
        }

        public void Validate()
        {
            string errormsg = "";

            //TODO: Have these check for the files on the system.
            if(SoundController.GetSoundEffect(Options.SoundMenuCancelFilename) == null)
            {
                errormsg += "Could not find file \"" + Options.SoundMenuCancelFilename + "\" for path sound. ";
            }
            if(SoundController.GetSoundEffect(Options.SoundMenuMoveFilename) == null)
            {
                errormsg += "Could not find file \"" + Options.SoundMenuMoveFilename + "\" for path sound. ";
            }
            if(SoundController.GetSoundEffect(Options.SoundMenuSelectFilename) == null)
            {
                errormsg += "Could not find file \"" + Options.SoundMenuSelectFilename + "\" for path sound. ";
            }
            if(SoundController.GetSoundEffect(Options.SoundMenuOpenFilename) == null)
            {
                errormsg += "Could not find file \"" + Options.SoundMenuOpenFilename + "\" for path sound. ";
            }
            

            if (errormsg.Length > 0)
            {
                throw new FileNotFoundException(errormsg);
            }
        }
    }
}
