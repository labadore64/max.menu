﻿using max.option;
using System;

namespace max.menu.component
{
    /// <summary>
    /// <para>A class that represents a single menu option.</para>
    /// </summary>
    public class MenuOption
    {
        /// <summary>
        /// <para>The text for the menu option.</para>
        /// </summary>
        /// <value>Text</value>
        public string Text { get; set; }

        /// <summary>
        /// The description of this menu option, such as a subtitle.
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Returns the accessibility string text for the TTS.
        /// </summary>
        public string AccessibleText {get { if (MenuOptions.AccessibleFullLabel) { return Text + ": " + Description; } return Text; } }

        /// <summary>
        /// <para>The action that is executed when selected.</para>
        /// <para>This can be any method that takes 0 parameters.</para>
        /// </summary>
        private Action MenuAction { get; set; }
        /// <summary>
        /// Initializes the menu option with no on-execute effect.
        /// </summary>
        /// <param name="Text">Menu text</param>
        public MenuOption(string Text)
        {
            this.Text = Text;
            MenuAction = null;
        }
        /// <summary>
        /// Initializes the menu option with an on-execute effect.
        /// </summary>
        /// <param name="Text">Menu text</param>
        /// <param name="MenuAction">Parameterless method to execute</param>
        public MenuOption(string Text, Action MenuAction)
        {
            this.Text = Text;
            this.MenuAction = MenuAction;
        }
        /// <summary>
        /// Initializes the menu option with no on-execute effect, and specific accessibility text.
        /// </summary>
        /// <param name="Text">Menu text</param>
        /// <param name="Description">Description</param>
        public MenuOption(string Text, string Description)
        {
            this.Text = Text;
            this.Description = Description;
            MenuAction = null;
        }
        /// <summary>
        /// Initializes the menu option with an on-execute effect, and specific accessibility text.
        /// </summary>
        /// <param name="Text">Menu text</param>
        /// <param name="Description">Description</param>
        /// <param name="MenuAction">Parameterless method to execute</param>
        public MenuOption(string Text, string Description, Action MenuAction)
        {
            this.Text = Text;
            this.Description = Description;
            this.MenuAction = MenuAction;
        }

        /// <summary>
        /// <para>Executes the option's effect when selected.</para>
        /// </summary>
        public void ExecuteOption()
        {
            if (MenuAction != null)
            {
                MenuAction.Invoke();
            }
        }

        public bool CompareAction(Action a)
        {
            return a.Equals(MenuAction);
        }
    }
}
