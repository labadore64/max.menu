﻿using max.input;
using max.lang;
using System;
using System.Collections.Generic;

namespace max.menu.component
{
    /// <summary>
    /// <para>A class that can be inherited to create accessible menus easily.</para>
    /// </summary>
    public abstract class Menu
    {

        protected Dictionary<string, Action> Actions { get; set; } = new Dictionary<string, Action>();

        public MenuController MenuController { get; protected set; }

        protected InputController InputController{ 
            get
            {
                return MenuController.InputController;
            }  
        }

        /// <summary>
        /// The amount of items in the menu.
        /// </summary>
        /// <value>Option Count</value>
        public int Count {
            get {
                return MenuOptions.Count;
            }
        }

        /// <summary>
        /// Actions that should ignore the select sound from playing, to prevent conflicts
        /// of sound effects.
        /// Cancel is treated separately from this array and is always ignored no matter what.
        /// </summary>
        /// <value>Array of ignored actions</value>
        public Action[] IgnoreSelectSoundArray { get; set; } = new Action[0];

        protected bool AutomateSelectSound { get; set; } = true;

        /// <summary>
        /// Whether or not the menu has been disposed of.
        /// </summary>
        /// <value>Disposed</value>
        public bool Disposed { get; protected set; } = false;

        /// <summary>
        /// The title of the menu.
        /// </summary>
        /// <value>Title</value>
        public string Title
        {
            get; set;
        }

        /// <summary>
        /// <para>Contains the menu selection options.</para>
        /// </summary>
        /// <value>Option list</value>
        protected List<MenuOption> MenuOptions = new List<MenuOption>();

        /// <summary>
        /// <para>Whether or not the text should wrap around.</para>
        /// </summary>
        /// <value>Enabled/Disabled</value>
        public bool Wrap { get; protected set; }
        /// <summary>
        /// <para>The currently selected index of the menu.</para>
        /// </summary>
        /// <value>Index</value>
        public int SelectedIndex = 0;
        /// <summary>
        /// <para>How many menu options should be skipped when pressing left or right.</para>
        /// </summary>
        /// <value>Amount</value>
        public int SkipAmount { get; protected set; } = 1;
        /// <summary>
        /// <para>Holds a reference for the parent menu (if need be).</para>
        /// </summary>
        /// <value>Menu</value>
        public Menu Parent { get; protected set; }

        /// <summary>
        /// Initializes the menu with no parent or options.
        /// </summary>
        public Menu()
        {

        }
        /// <summary>
        /// Initializes the menu with no options and a reference to a parent.
        /// </summary>
        /// <param name="Parent">The parent menu</param>
        public Menu(Menu Parent)
        {
            this.Parent = Parent;
        }
        /// <summary>
        /// Initializes the menu with no parent but with options.
        /// </summary>
        /// <param name="Options">Menu options</param>
        public Menu(List<MenuOption> Options)
        {
            for (int i = 0; i < Options.Count; i++) {
                MenuOptions.Add(Options[i]);
            }
        }

        /// <summary>
        /// Initializes the menu with no parent but with options.
        /// </summary>
        /// <param name="Options">Menu options</param>
        public Menu(MenuOption[] Options)
        {
            for (int i = 0; i < Options.Length; i++)
            {
                MenuOptions.Add(Options[i]);
            }
        }

        /// <summary>
        /// Initializes the menu with no parent but with options.
        /// </summary>
        /// <param name="Options">Menu options</param>
        /// <param name="Parent">Parent menu</param>
        public Menu(MenuOption[] Options, Menu Parent)
        {
            for (int i = 0; i < Options.Length; i++) { 
                MenuOptions.Add(Options[i]);
            }
            this.Parent = Parent;
        }

    /// <summary>
    /// Initializes the menu with no parent but with options.
    /// </summary>
    /// <param name="Options">Menu options</param>
    /// <param name="Parent">Parent menu</param>
    public Menu(List<MenuOption> Options, Menu Parent)
    {
        for (int i = 0; i < Options.Count; i++) {
            MenuOptions.Add(Options[i]);
        }
        this.Parent = Parent;
    } 

        /// <summary>
        /// <para>Call this method when initializing a menu.</para>
        /// </summary>
        public virtual void Initialize(MenuController Controller)
        {
            MenuController = Controller;
            string title = LanguageManager.Get(GetType().Name, "title");
            if(title != null)
            {
                Title = title;
            }
        }

        /// <summary>
        /// <para>Performs all the input checks and updates the state of the menu.</para>
        /// <para>Call this method every cycle when the menu is active.</para>
        /// </summary>
        public virtual void Update()
        {

                if (InputController.TestSelectPressed())
                {
                    ActionSelect();
                }
                else if (InputController.TestCancelPressed())
                {
                    ActionCancel();
                }
                else if (InputController.TestUpPressed())
                {
                    ActionUp();
                }
                else if (InputController.TestDownPressed())
                {
                    ActionDown();
                }
                else if (InputController.TestLeftPressed())
                {
                    ActionLeft();
                }
                else if (InputController.TestRightPressed())
                {
                    ActionRight();
                }

        
        }

        /// <summary>
        /// <para>Adds a menu option with no effect when pressed.</para>
        /// </summary>
        /// <param name="Text">The text of the menu</param>
        public void AddMenuOption(string Text)
        {
            MenuOptions.Add(new MenuOption(Text));
        }
        /// <summary>
        /// <para>Adds a menu option that has the effect of a method with no parameters when passed.</para>
        /// </summary>
        /// <param name="Text">The text of the menu</param>
        /// <param name="MenuAction">Parameterless method to execute</param>
        public void AddMenuOption(string Text, Action MenuAction)
        {
            MenuOptions.Add(new MenuOption(Text, MenuAction));
        }
        /// <summary>
        /// <para>Adds a menu option that has the effect of a method with no parameters when passed, with additional descriptive text.</para>
        /// </summary>
        /// <param name="Text">The text of the menu</param>
        /// <param name="AccessibleText">The accessible menu text</param>
        /// <param name="MenuAction">Parameterless method to execute</param>
        public void AddMenuOption(string Text, string Description, Action MenuAction)
        {
            MenuOptions.Add(new MenuOption(Text, Description, MenuAction));
        }

        /// <summary>
        /// <para>Adds a menu option with additional accessible descriptive text but no effect when pressed.</para>
        /// </summary>
        /// <param name="Text">The text of the menu</param>
        /// <param name="AccessibleText">The accessible menu text</param>
        public void AddMenuOption(string Text, string Description)
        {
            MenuOptions.Add(new MenuOption(Text, Description));
        }

        /// <summary>
        /// <para>Activates the "selected" action.</para>
        /// </summary>
        protected virtual void ActionSelect()
        {
            if (MenuOptions.Count > 0)
            {
                if (MenuOptions[SelectedIndex] != null)
                {
                    MenuOptions[SelectedIndex].ExecuteOption();
                    if (!MenuOptions[SelectedIndex].CompareAction(Cancel))
                    {
                        bool DoSound = true;

                        for(int i = 0; i < IgnoreSelectSoundArray.Length; i++) { 
                            if (MenuOptions[SelectedIndex].CompareAction(IgnoreSelectSoundArray[i]))
                            {
                                DoSound = false;
                            }
                        }
                        if (DoSound)
                        {
                            PlaySelectSound();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// <para>Activates the "canceled" action.</para>
        /// </summary>
        protected virtual void ActionCancel()
        {
            Cancel();
        }

        /// <summary>
        /// <para>Activates the "up" action.</para>
        /// </summary>
        protected virtual void ActionUp()
        {
            ListDecrease(1);
            PlayMoveSound();
        }
        /// <summary>
        /// <para>Activates the "down" action.</para>
        /// </summary>
        protected virtual void ActionDown()
        {
            ListIncrease(1);
            PlayMoveSound();
        }
        /// <summary>
        /// <para>Activates the "left" action.</para>
        /// </summary>
        protected virtual void ActionLeft()
        {
            ListDecrease(SkipAmount);
            
        }
        /// <summary>
        /// <para>Activates the "right" action.</para>
        /// </summary>
        protected virtual void ActionRight()
        {
            ListIncrease(SkipAmount);
            PlayMoveSound();
        }
        /// <summary>
        /// <para>Moves down the list by a specified amount.</para>
        /// </summary>
        /// <param name="amount">The amount to move down the list</param>
        protected void ListDecrease(int amount)
        {
            int size = MenuOptions.Count;
            SelectedIndex-=amount;
            if (SelectedIndex < 0)
            {
                if (Wrap)
                {
                    SelectedIndex = size - 1;
                }
                else
                {
                    SelectedIndex = 0;
                }
            }
        }
        /// <summary>
        /// <para>Moves down the list by a specified amount.</para>
        /// </summary>
        /// <param name="amount">The amount to move up the list</param>
        protected void ListIncrease(int amount)
        {
            int size = MenuOptions.Count;
            SelectedIndex += amount;
            if (SelectedIndex >= size)
            {
                if (!Wrap)
                {
                    SelectedIndex = size - 1;
                }
                else
                {
                    SelectedIndex = 0;
                }
            }
        }

        /// <summary>
        /// You should execute this when you're about to destroy this object.
        /// </summary>
        public virtual void Dispose()
        {
            Disposed = true;
        }

        protected void PlayOpenSound()
        {
            if (MenuController.Options.MenuSoundsEnabledAlways)
            {
                MenuController.SoundController.PlaySound(MenuController.Options.SoundMenuOpenFilename);
            }
        }

        protected void PlayMoveSound()
        {
            if (MenuController.Options.MenuSoundsEnabledAlways)
            {
                MenuController.SoundController.PlaySound(MenuController.Options.SoundMenuMoveFilename);
            }
        }

        protected void PlaySelectSound()
        {
            if (MenuController.Options.MenuSoundsEnabledAlways)
            {
                MenuController.SoundController.PlaySound(MenuController.Options.SoundMenuSelectFilename);
            }
        }

        protected void PlayCancelSound()
        {
            if (MenuController.Options.MenuSoundsEnabledAlways)
            {
                MenuController.SoundController.PlaySound(MenuController.Options.SoundMenuCancelFilename);
            }
        }

        /// <summary>
        /// This method is for a generic cancel button effect.
        /// </summary>
        public virtual void Cancel()
        {
            Menu m = MenuController.Pop();
            //if successful play the cancel sound
            if (m != null)
            {
                PlayCancelSound();
            }
        }

        /// <summary>
        /// Triggers when a menu is deactivated, such as when a submenu is opened.
        /// </summary>
        public virtual void Deactivate()
        {

        }

        /// <summary>
        /// Triggers when a menu is reactivated, such as when a submenu is closed.
        /// </summary>
        public virtual void Reactivate()
        {

        }


        public abstract void UpdateMenuList();

        protected virtual void GenerateOptions()
        {
            string name;
            string desc;
            foreach (KeyValuePair<string,Action> a in Actions){
                name = LanguageManager.Get(GetType().Name, a.Key);
                if (name != null)
                {
                    desc = LanguageManager.Get(GetType().Name, a.Key + "_desc");

                    if (desc != null)
                    {
                        AddMenuOption(LanguageManager.Get(GetType().Name,a.Key), desc, a.Value);
                    }
                    else
                    {
                        AddMenuOption(LanguageManager.Get(GetType().Name, a.Key), a.Value);
                    }
                } else
                {
                    AddMenuOption("TRANSLATION KEY IS MISSING FOR " + a.Key + "!", a.Value);
                }
            }
        }

    }
}