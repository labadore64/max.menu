﻿using System;

namespace max.option
{
    public sealed class MenuOptions
    {
        /// <summary>
        /// The location of the menu cancel sound asset. Plays when cancelling out of a menu.
        /// </summary>
        /// <value>Path</value>
        public String SoundMenuCancelFilename = "maxseer\\menu_cancel";

        /// <summary>
        /// The location of the menu move sound asset. Plays when moving through a menu.
        /// </summary>
        /// <value>Path</value>
        public String SoundMenuMoveFilename = "maxseer\\menu_move";


        /// <summary>
        /// The location of the menu select sound asset. Plays when selecting an option on a menu.
        /// </summary>
        /// <value>Path</value>
        public String SoundMenuSelectFilename = "maxseer\\menu_move";


        /// <summary>
        /// The location of the menu select sound asset. Plays when selecting an option on a menu.
        /// </summary>
        /// <value>Path</value>
        public String SoundMenuOpenFilename = "maxseer\\menu_move";


        /// <summary>
        /// Whether or not the menu sounds will always play, or whether 
        /// they will only play when the maxseer engine is enabled.
        /// If it is true, it is always enabled, if it is false, it is 
        /// only playing sounds when maxseer is enabled.
        /// </summary>
        /// <value>Enabled/Disabled</value>
        public bool MenuSoundsEnabledAlways = true;

        /// <summary>
        /// If true, AccessibleText returns the text and description.
        /// If false, AccessibleText returns the same thing as text.
        /// </summary>
        public static bool AccessibleFullLabel { get; set; } = true;
    }
}
